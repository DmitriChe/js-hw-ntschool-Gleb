function getNumbersInput() {
    const numbersArray = [
        parseInt(document.querySelector('.numberone').value), 
        parseInt(document.querySelector('.numbertwo').value)
    ]

    return numbersArray;
}

function displayResultNumber(resultMultiplNumber) {
    document.querySelector('.result').innerHTML = 'Результат умножения: ' + resultMultiplNumber;
}

function multiplNumbers(numArray) {
    const multiplResult = numArray[0] * numArray[1];

    return multiplResult;
}

function clickButtonEvent() {
    const multiplResult = multiplNumbers(getNumbersInput());

    displayResultNumber(multiplResult);
}

document.querySelector('.submit').onclick = clickButtonEvent;
